const onSuccess = () => {
  if (document) {
    document.getElementById('status').innerText = 'Permission Granted!';
  }
}

const onError = () => {
  if (document) {
    document.getElementById('status').innerText = 'Permission Denied :(';
  }
}

const askPermission = () => {
  navigator.mediaDevices
    .getUserMedia({ audio: true })
    .then((stream) => {
      onSuccess();
    })
    .catch((err) => {
      document.getElementById('error').innerText = err;
      onError()
    });
};

const askPermissionNew = () => {
  navigator.permissions.query({ name: "microphone" }).then((result) => {
    if (result.state === "granted") {
      document.getElementById('PermissionAPI').innerText = 'PermissionAPI Status: Granted!';
      return;
    } else if (result.state === "prompt") {
      document.getElementById('PermissionAPI').innerText = 'PermissionAPI Status: Prompted!';
      return;
    }
    // Don't do anything if the permission was denied.
    document.getElementById('PermissionAPI').innerText = 'PermissionAPI Status: Blocked!';
  });
}

window.addEventListener('load', () => {
  askPermission();
  askPermissionNew();
});

